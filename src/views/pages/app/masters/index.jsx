import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Badge,
  Input,
  Button,
} from "reactstrap";
import DataTable from "react-data-table-component";
import { Star, Search } from "react-feather";
import * as Icon from "react-feather"

const CustomHeader = (props) => {
  return (
    <div className="d-flex flex-wrap justify-content-between">
      <div className="add-new">
        <Button.Ripple color="primary">Add New</Button.Ripple>
      </div>
      <div className="position-relative has-icon-left mb-1">
        <Input value={props.value} onChange={(e) => props.handleFilter(e)} />
        <div className="form-control-position">
          <Search size="15" />
        </div>
      </div>
    </div>
  );
};

class DataTableCustom extends React.Component {
  state = {
    columns: [
      {
        name: "Name",
        selector: "name",
        sortable: true,
        minWidth: "200px",
        cell: (row) => (
          <div className="d-flex flex-xl-row flex-column align-items-xl-center align-items-start py-xl-0 py-1">
            <div className="user-img ml-xl-0 ml-2">
              <img
                className="img-fluid rounded-circle"
                height="36"
                width="36"
                src={row.image}
                alt={row.name}
              />
            </div>
            <div className="user-info text-truncate ml-xl-50 ml-0">
              <span
                title={row.name}
                className="d-block text-bold-500 text-truncate mb-0"
              >
                {row.name}
              </span>
            </div>
          </div>
        ),
      },
      {
        name: "Gender",
        selector: "gender",
        sortable: true,
        cell: (row) => (
          <p className="text-bold-500 text-truncate mb-0">{row.gender}</p>
        ),
      },
      {
        name: "Status",
        selector: "status",
        sortable: true,
        cell: (row) => (
          <Badge
            color={row.status === "inactive" ? "light-danger" : "light-success"}
            pill
          >
            {row.status}
          </Badge>
        ),
      },
      {
        name: "Service",
        selector: "service",
        sortable: true,
        cell: (row) => <p className="text-bold-500 mb-0">{row.service}</p>,
      },
      {
        name: "Actions",
        selector: "",
        sortable: true,
        cell: (row) => {
          return (
            <div className="d-flex flex-column align-items-center">
              <ul className="list-inline mb-0">
                <li className="list-inline-item">
                  <Icon.Edit size={20} />
                </li>
                <li className="list-inline-item">
                  <Icon.Delete size={20} />
                </li>
              </ul>
            </div>
          );
        },
      },
    ],
    data: [
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-2.jpg"),
        name: "Alyss Lillecrop",
        gender:'Famale',
        service:'Service 1',
        email: "alillecrop0@twitpic.com",
        date: "May 13, 2018",
        status: "active",
        revenue: "$32,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-1.jpg"),
        name: "Shep Pentlow",
        gender:'Male',
        service:'Service 7',
        email: "spentlow1@home.pl",
        date: "June 5, 2019",
        status: "active",
        revenue: "$50,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-3.jpg"),
        name: "Gasper Morley",
        gender:'Male',
        service:'Service 4',
        email: "gmorley2@chronoengine.com",
        date: "December 24, 2019",
        status: "active",
        revenue: "$78,000",
        ratings: "average",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-4.jpg"),
        name: "Phaedra Jerrard",
        gender:'Female',
        service:'Service 23',
        email: "pjerrard3@blogs.com",
        date: "November 30, 2018",
        status: "inactive",
        revenue: "$10,000",
        ratings: "bad",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-5.jpg"),
        name: "Conn Plose",
        gender:'Male',
        service:'Service 1',
        email: "cplose4@geocities.com",
        date: "April 8, 2017",
        status: "active",
        revenue: "$22,000",
        ratings: "average",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-6.jpg"),
        name: "Tootsie Brandsma",
        gender:'Female',
        service:'Service 11',
        email: "tbrandsma5@theatlantic.com",
        date: "August 12, 2019",
        status: "inactive",
        revenue: "$49,000",
        ratings: "bad",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-8.jpg"),
        name: "Sibley Bum",
        gender:'Female',
        service:'Service 1',
        email: "sbum6@sourceforge.net",
        date: "October 1, 2017",
        status: "active",
        revenue: "$56,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-7.jpg"),
        name: "Kristoffer Thew",
        gender:'Male',
        service:'Service 1',
        email: "kthew7@amazon.com",
        date: "February 28, 2018",
        status: "inactive",
        revenue: "$83,000",
        ratings: "bad",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-26.jpg"),
        name: "Fay Hasard",
        gender:'Female',
        service:'Service 9',
        email: "fhasard8@java.com",
        date: "January 29, 2018",
        status: "active",
        revenue: "$26,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-12.jpg"),
        name: "Tabby Abercrombie",
        gender:'Female',
        service:'Service 2',
        email: "tabercrombie9@statcounter.com",
        date: "April 1, 2019",
        status: "active",
        revenue: "$60,000",
        ratings: "average",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-10.jpg"),
        name: "	Stella Indruch",
        gender:'Female',
        service:'Service 4',
        email: "sindruch1@mayoclinic.com",
        date: "Dec 4, 2019",
        status: "active",
        revenue: "$21,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-17.jpg"),
        name: "	Aron McNirlin",
        gender:'Male',
        service:'Service 8',
        email: "amcnirlin2@samsung.com",
        date: "Jan 4, 2018",
        status: "inactive",
        revenue: "$30,000",
        ratings: "bad",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-20.jpg"),
        name: "Ange Trenholm",
        gender:'Female',
        service:'Service 5',
        email: "atrenholm4@slideshare.net	",
        date: "February 23, 2019",
        status: "active",
        revenue: "$12,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-14.jpg"),
        name: "Caterina Starkie",
        gender:'Female',
        service:'Service 10',
        email: "cstarkie5@feedburner.com",
        date: "September 8, 2018",
        status: "active",
        revenue: "$40,000",
        ratings: "average",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-25.jpg"),
        name: "Hugibert McGeagh",
        gender:'Male',
        service:'Service 1',
        email: "hmcgeaghf@smh.com.au",
        date: "August 20, 2017",
        status: "active",
        revenue: "$90,000",
        ratings: "good",
      },
    ],
    filteredData: [],
    value: "",
  };

  handleFilter = (e) => {
    let value = e.target.value;
    let data = this.state.data;
    let filteredData = this.state.filteredData;
    this.setState({ value });

    if (value.length) {
      filteredData = data.filter((item) => {
        let startsWithCondition =
          item.name.toLowerCase().startsWith(value.toLowerCase()) ||
          item.date.toLowerCase().startsWith(value.toLowerCase()) ||
          item.email.toLowerCase().startsWith(value.toLowerCase()) ||
          item.revenue.toLowerCase().startsWith(value.toLowerCase()) ||
          item.status.toLowerCase().startsWith(value.toLowerCase());
        let includesCondition =
          item.name.toLowerCase().includes(value.toLowerCase()) ||
          item.date.toLowerCase().includes(value.toLowerCase()) ||
          item.email.toLowerCase().includes(value.toLowerCase()) ||
          item.revenue.toLowerCase().includes(value.toLowerCase()) ||
          item.status.toLowerCase().includes(value.toLowerCase());

        if (startsWithCondition) {
          return startsWithCondition;
        } else if (!startsWithCondition && includesCondition) {
          return includesCondition;
        } else return null;
      });
      this.setState({ filteredData });
    }
  };

  render() {
    let { data, columns, value, filteredData } = this.state;
    return (
      <Card>
        <CardHeader>
          <CardTitle>Masters</CardTitle>
        </CardHeader>
        <CardBody className="rdt_Wrapper">
          <DataTable
            className="dataTable-custom"
            data={value.length ? filteredData : data}
            columns={columns}
            noHeader
            pagination
            subHeader
            subHeaderComponent={
              <CustomHeader value={value} handleFilter={this.handleFilter} />
            }
          />
        </CardBody>
      </Card>
    );
  }
}

export default DataTableCustom;
