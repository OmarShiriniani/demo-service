import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Badge,
  Input,
  Button,
} from "reactstrap";
import DataTable from "react-data-table-component";
import { Star, Search } from "react-feather";
import * as Icon from "react-feather"

const CustomHeader = (props) => {
  return (
    <div className="d-flex flex-wrap justify-content-between">
      <div className="add-new">
        <Button.Ripple color="primary">Add New</Button.Ripple>
      </div>
      <div className="position-relative has-icon-left mb-1">
        <Input value={props.value} onChange={(e) => props.handleFilter(e)} />
        <div className="form-control-position">
          <Search size="15" />
        </div>
      </div>
    </div>
  );
};

class DataTableCustom extends React.Component {
  state = {
    columns: [
      {
        name: "Title",
        selector: "name",
        sortable: true,
        minWidth: "200px",
        cell: (row) => (
          <div className="d-flex flex-xl-row flex-column align-items-xl-center align-items-start py-xl-0 py-1">
            {/* <div className="user-img ml-xl-0 ml-2">
              <img
                className="img-fluid rounded-circle"
                height="36"
                width="36"
                src={row.image}
                alt={row.name}
              />
            </div> */}
            <div className="user-info text-truncate ml-xl-50 ml-0">
              <span
                title={row.name}
                className="d-block text-bold-500 text-truncate mb-0"
              >
                {row.name}
              </span>
              {/* <small title={row.email}>{row.email}</small> */}
            </div>
          </div>
        ),
      },
    //   {
    //     name: "Date Created",
    //     selector: "date",
    //     sortable: true,
    //     cell: (row) => (
    //       <p className="text-bold-500 text-truncate mb-0">{row.date}</p>
    //     ),
    //   },
      {
        name: "Status",
        selector: "status",
        sortable: true,
        cell: (row) => (
          <Badge
            color={row.status === "inactive" ? "light-danger" : "light-success"}
            pill
          >
            {row.status}
          </Badge>
        ),
      },
      {
        name: "Time",
        selector: "time",
        sortable: true,
        cell: (row) => <p className="text-bold-500 mb-0">{row.time}</p>,
      },
      {
        name: "Actions",
        selector: "",
        sortable: true,
        cell: (row) => {
          return (
            <div className="d-flex flex-column align-items-center">
              <ul className="list-inline mb-0">
                <li className="list-inline-item">
                <Icon.Edit size={20} />
                </li>
                <li className="list-inline-item">
                <Icon.Delete size={20} />
                </li>
              </ul>
            </div>
          );
        },
      },
    ],
    data: [
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-2.jpg"),
        name: "Service 1",
        email: "alillecrop0@twitpic.com",
        date: "May 13, 2018",
        time:'2 Hours',
        status: "active",
        revenue: "$32,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-1.jpg"),
        name: "Service 2",
        email: "spentlow1@home.pl",
        date: "June 5, 2019",
        status: "active",
        time:'2 Hours',
        revenue: "$50,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-3.jpg"),
        name: "Service 3",
        email: "gmorley2@chronoengine.com",
        date: "December 24, 2019",
        status: "active",
        time:'2 Hours',
        revenue: "$78,000",
        ratings: "average",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-4.jpg"),
        name: "Service 4",
        email: "pjerrard3@blogs.com",
        date: "November 30, 2018",
        status: "inactive",
        time:'2 Hours',
        revenue: "$10,000",
        ratings: "bad",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-5.jpg"),
        name: "Service 5",
        email: "cplose4@geocities.com",
        date: "April 8, 2017",
        status: "active",
        time:'1 Hours',
        revenue: "$22,000",
        ratings: "average",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-6.jpg"),
        name: "Service 6",
        email: "tbrandsma5@theatlantic.com",
        date: "August 12, 2019",
        status: "inactive",
        time:'30 Minutes',
        revenue: "$49,000",
        ratings: "bad",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-8.jpg"),
        name: "Service 7",
        email: "sbum6@sourceforge.net",
        date: "October 1, 2017",
        status: "active",
        time:'2 Minutes',
        revenue: "$56,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-7.jpg"),
        name: "Service 8",
        email: "kthew7@amazon.com",
        date: "February 28, 2018",
        status: "inactive",
        time:'3 Hours',
        revenue: "$83,000",
        ratings: "bad",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-26.jpg"),
        name: "Service 9",
        email: "fhasard8@java.com",
        date: "January 29, 2018",
        status: "active",
        time:'2 Hours',
        revenue: "$26,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-12.jpg"),
        name: "Service 10",
        email: "tabercrombie9@statcounter.com",
        date: "April 1, 2019",
        status: "active",
        time:'1 Hours',
        revenue: "$60,000",
        ratings: "average",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-10.jpg"),
        name: "Service 11",
        email: "sindruch1@mayoclinic.com",
        date: "Dec 4, 2019",
        status: "active",
        time:'4 Hours',
        revenue: "$21,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-17.jpg"),
        name: "Service 12",
        email: "amcnirlin2@samsung.com",
        date: "Jan 4, 2018",
        status: "inactive",
        time:'4 Hours',
        revenue: "$30,000",
        ratings: "bad",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-20.jpg"),
        name: "Service 13",
        email: "atrenholm4@slideshare.net	",
        date: "February 23, 2019",
        status: "active",
        time:'30 Minutes',
        revenue: "$12,000",
        ratings: "good",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-14.jpg"),
        name: "Service 14",
        email: "cstarkie5@feedburner.com",
        date: "September 8, 2018",
        status: "active",
        time:'30 Minutes',
        revenue: "$40,000",
        ratings: "average",
      },
      {
        image: require("../../../../assets/img/portrait/small/avatar-s-25.jpg"),
        name: "Service 15",
        email: "hmcgeaghf@smh.com.au",
        date: "August 20, 2017",
        status: "active",
        time:'30 Minutes',
        revenue: "$90,000",
        ratings: "good",
      },
    ],
    filteredData: [],
    value: "",
  };

  handleFilter = (e) => {
    let value = e.target.value;
    let data = this.state.data;
    let filteredData = this.state.filteredData;
    this.setState({ value });

    if (value.length) {
      filteredData = data.filter((item) => {
        let startsWithCondition =
          item.name.toLowerCase().startsWith(value.toLowerCase()) ||
          item.date.toLowerCase().startsWith(value.toLowerCase()) ||
          item.email.toLowerCase().startsWith(value.toLowerCase()) ||
          item.revenue.toLowerCase().startsWith(value.toLowerCase()) ||
          item.status.toLowerCase().startsWith(value.toLowerCase());
        let includesCondition =
          item.name.toLowerCase().includes(value.toLowerCase()) ||
          item.date.toLowerCase().includes(value.toLowerCase()) ||
          item.email.toLowerCase().includes(value.toLowerCase()) ||
          item.revenue.toLowerCase().includes(value.toLowerCase()) ||
          item.status.toLowerCase().includes(value.toLowerCase());

        if (startsWithCondition) {
          return startsWithCondition;
        } else if (!startsWithCondition && includesCondition) {
          return includesCondition;
        } else return null;
      });
      this.setState({ filteredData });
    }
  };

  render() {
    let { data, columns, value, filteredData } = this.state;
    return (
      <Card>
        <CardHeader>
          <CardTitle>Services</CardTitle>
        </CardHeader>
        <CardBody className="rdt_Wrapper">
          <DataTable
            className="dataTable-custom"
            data={value.length ? filteredData : data}
            columns={columns}
            noHeader
            pagination
            subHeader
            subHeaderComponent={
              <CustomHeader value={value} handleFilter={this.handleFilter} />
            }
          />
        </CardBody>
      </Card>
    );
  }
}

export default DataTableCustom;
